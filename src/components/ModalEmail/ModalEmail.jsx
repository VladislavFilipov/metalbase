import React, { useState } from 'react';
import axios from 'axios';

import './ModalEmail.scss';

function ModalEmail({ closeModal }) {

    let [status, setStatus] = useState('fill');

    let sendData = (e) => {
        e.preventDefault();
        const form = e.currentTarget;

        if (
            !form.elements.email.value ||
            !form.elements.ask.value ||
            !form.elements.policy.checked
        ) {
            setStatus('error');
            return;
        }

        console.log({
            name: form.elements.email.value,
            tel: form.elements.ask.value
        })

        axios.post('https://metalbase-server.herokuapp.com/send-mail-ask', {
            email: form.elements.email.value,
            ask: form.elements.ask.value
        })
            .then(res => {
                console.log('успех')
                setStatus('success');
            })
            .catch(err => console.log(err))
    }

    return (
        <div className="ModalEmail">
            <form className="ModalEmail__form" onSubmit={sendData}>
                <h3>Задать вопрос</h3>
                <input type="email" name="email" placeholder="Введите вашу почту" />
                <textarea name="ask" placeholder="Введите ваш вопрос"></textarea>
                <label>
                    <input type="checkbox" name="policy" />
                    <span>Согласен с политикой конфиденциальности</span>
                </label>
                {status === 'error' && <p>Заполните все поля!</p>}
                {status !== 'success' && <button type="submit">Отправить</button>}
                {status === 'success' && <p>Успешно!</p>}

                <div className="close" onClick={() => closeModal()}>+</div>
            </form>
        </div>
    );
}

export default ModalEmail;
