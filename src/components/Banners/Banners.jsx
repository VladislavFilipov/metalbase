import React, { useState, useEffect } from 'react';

import './Banners.scss';

import ModalCall from '../ModalCall/ModalCall';

const banners = [
    '/static/banners/banner_1.jpg',
    '/static/banners/banner_2.jpg',
    '/static/banners/banner_3.jpg',
    '/static/banners/banner_4.jpg',

];

let timer;

function Banners() {

    let [showModalCall, setShowModalCall] = useState(false);

    useEffect(() => {
        timer = setInterval(() => {
            setPosition(p => p + 1 >= banners.length ? 0 : ++p)
        }, 5000);
    }, []);


    let [position, setPosition] = useState(0);
    useEffect(() => {


        const
            slider = getComputedStyle(document.querySelector('.Banners__slider')).display !== 'none' ?
                document.querySelector('.Banners__slider') :
                document.querySelector('.mobile'),
            moveBox = slider.querySelector('.move-box'),
            btnBack = slider.querySelector('.btn-back'),
            btnFwd = slider.querySelector('.btn-fwd');

        let sliderWidth = parseFloat(getComputedStyle(slider).width);

        moveBox.style.transform = `translateX(-${sliderWidth * position}px)`;

        if (position === 0) btnBack.classList.add('hide');
        else btnBack.classList.remove('hide');

        if (position === banners.length - 1) btnFwd.classList.add('hide');
        else btnFwd.classList.remove('hide');

        clearInterval(timer);
        timer = setInterval(() => {
            setPosition(p => p + 1 >= banners.length ? 0 : ++p)
        }, 5000);

    }, [position]);

    return (
        <div className="Banners">
            {showModalCall && <ModalCall closeModal={() => setShowModalCall(false)} />}
            <div className="Banners__slider">
                <div className="btn-fwd" onClick={() => setPosition(position + 1 >= banners.length ? position : ++position)}>
                    <i className="fas fa-chevron-right"></i>
                </div>
                <div className="btn-back" onClick={() => setPosition(position - 1 < 0 ? position : --position)}>
                    <i className="fas fa-chevron-left"></i>
                </div>
                <div className="move-box">
                    {banners.map((banner, i) => (
                        <div className="slide" key={i}>
                            <div>
                                <img onClick={() => setShowModalCall(true)} src={`${process.env.REACT_APP_SERVER_URL}${banner}`} alt="" />
                            </div>
                        </div>
                    ))}
                </div>
            </div>

            <div className="Banners__static-banner">
                <img onClick={() => setShowModalCall(true)} src={`${process.env.REACT_APP_SERVER_URL}/static/banners/banner_5.jpg`} alt="" />
            </div>

            <div className="Banners__slider mobile">
                <div className="btn-fwd" onClick={() => setPosition(position + 1 >= banners.length ? position : ++position)}>
                    <i className="fas fa-chevron-right"></i>
                </div>
                <div className="btn-back" onClick={() => setPosition(position - 1 < 0 ? position : --position)}>
                    <i className="fas fa-chevron-left"></i>
                </div>
                <div className="move-box">
                    {banners.map((banner, i) => (
                        <div className="slide" key={i}>
                            <div>
                                <img onClick={() => setShowModalCall(true)} src={`${process.env.REACT_APP_SERVER_URL}${banner}`} alt="" />
                            </div>
                        </div>
                    ))}
                    <div className="slide">
                        <div>
                            <img onClick={() => setShowModalCall(true)} src={`${process.env.REACT_APP_SERVER_URL}/static/banners/banner_5.jpg`} alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Banners;
