import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import ModalCall from '../ModalCall/ModalCall';
import ModalEmail from '../ModalEmail/ModalEmail';

import './Footer.scss';

function Footer() {

    let [showModalCall, setShowModalCall] = useState(false);
    let [showModalEmail, setShowModalEmail] = useState(false);

    return (
        <div className="Footer">
            {showModalCall && <ModalCall closeModal={() => setShowModalCall(false)} />}
            {showModalEmail && <ModalEmail closeModal={() => setShowModalEmail(false)} />}
            <div className="Footer__other">
                <div className="Footer__name">Металлобаза Волхонка</div>
                {/* <div className="Footer__site-map">Карта сайта</div> */}
                <div className="Footer__pr-policy"><Link to="/policy">Политика конфиденциальности</Link></div>
                <div className="Footer__toadmin"><Link to="/admin">Администраторам</Link></div>
                <div className="Footer__copyright">© 2019. Все права защищены.</div>

            </div>
            <div className="Footer__block2">
                <div className="Footer__phone"><a href="tel:+78123255055">+7 (812) 325-50-55</a><i className="fas fa-phone"></i></div>
                <button className="Footer__call" onClick={() => setShowModalCall(true)}>Заказать обратный звонок</button>
            </div>
            <div className="Footer__block3">
                <div className="Footer__mail"><a href="mailto:info@metallobazav.ru">info@metallobazav.ru</a><i className="far fa-envelope"></i></div>
                <button className="Footer__ask" onClick={() => setShowModalEmail(true)}>Задать вопрос</button>
            </div>
            <div className="Footer__block4">
                <div className="Footer__address">Санкт Петербург, Горелово, Волхонское шоссе, 6<i className="fas fa-map-marker-alt"></i></div>
                <div className="Footer__messengers">
                    <a target="_blank" rel="noopener noreferrer" href="https://msng.link/wa/79213619899">
                        <img alt="Whatsapp" src="https://img.icons8.com/color/48/000000/whatsapp.png" />
                    </a>
                    <a target="_blank" rel="noopener noreferrer" href="https://invite.viber.com/?g2=AQAPJkO4vleg%2F0rBCE2dAWGKoLPHpuuYq%2B0e%2B%2F9tvkUoLWhaLOzpg9sK%2Fr6hUSsP&lang=ru">
                        <img alt="Viber" src="https://img.icons8.com/color/48/000000/viber.png" />
                    </a>
                    <a target="_blank" rel="noopener noreferrer" href="https://msng.link/tg/metallobazav">
                        <img alt="Telegram" src="https://img.icons8.com/color/48/000000/telegram-app.png" />
                    </a>
                </div>
            </div>
        </div>
    );
}

export default Footer;
