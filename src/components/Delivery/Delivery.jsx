import React, { useState } from 'react';
import { Helmet } from "react-helmet";

// import MapComponent from './Map';
import Test from './Test';

import './Delivery.scss';

const transportList = [
    { name: 'Газель', rate: 40, redArea: 2500, blueArea: 3000 },
    { name: 'Пятитонник', rate: 55, redArea: 3900, blueArea: 4400 },
    { name: 'Шаланда', rate: 85, redArea: 6500, blueArea: 7500 },
    { name: 'Десятитонник', rate: 85, redArea: 6500, blueArea: 7500 },
    { name: 'Манипулятор*', rate: 100, redArea: 7500, blueArea: 8500 }
];

function round50(val) {
    const mod50 = val % 50;
    if (mod50 === 0) {
        return val;
    } else if (mod50 < 25) {
        return val - mod50;
    } else {
        return val - mod50 + 50;
    }
}

function calcPrice(area, km, transport) {
    if (area === 0) { // красная зона
        return transport.redArea;
    } else if (area === 1) { // синяя зона
        return transport.blueArea;
    } else if (area === 2) { // синяя зона за кадом
        return round50(transport.blueArea + (km * transport.rate));
    } else if (area === 3) { // красная зона за кадом
        return round50(transport.redArea + (km * transport.rate));
    }
}

function Delivery() {
    let [km, setKm] = useState();
    let [area, setArea] = useState();

    return (
        <div className="Delivery">
            <Helmet>
                <meta charSet="utf-8" />
                <title>Доставка</title>
                <meta name="fragment" content="!" />
            </Helmet>
            <h1 className="Delivery__title">Расчёт стоимости доставки металлопроката</h1>

            <p>Внутри красной и синей зоны цены на доставку фиксированные. За пределами КАД стоимость доставки считается по километражу.</p>
            <p><b>Укажите точку на карте или введите точный адрес доставки в поисковую строку.</b></p>

            <div className="Delivery__map">
                <Test returnKm={(value) => setKm(value)} returnArea={(value) => setArea(value)} />
            </div>
            {area === null && <p><b>Зона не обслуживается</b> </p>}
            {
                (
                    <ul className="Delivery__transport">
                        {transportList.map((transport, i) => (
                            <li key={i} className="Delivery__transport-item">
                                <div className="Delivery__transport-name">{transport.name}</div>
                                <div className="Delivery__transport-price">{calcPrice(area, km, transport) || (<span>&#8212;</span>)} руб.</div>
                            </li>
                        ))}
                    </ul>
                )
            }
            <p>"Металлобаза Волхонка" осуществляет доставку металлопроката по Санкт-Петербургу и Ленинградской области собственным автотранспортом.</p>
            <p>На данный момент компания располагает автопарком с разной грузоподъемностью: 1.5т, 5т, 15т и 20 т. Благодаря этому мы легко и точно в срок осуществим доставку на Ваш объект в Санкт-Петербурге или любом населенном пункте Ленинградской области.</p>
            <p className="warning">* До 3-х тн выгрузка бесплатно, после 3-х тн 500р за 1 тн. Уточняйте стоимость у оператора.</p>

        </div >
    );
}

export default Delivery;

