import React, { useState, useEffect } from 'react';
import axios from 'axios';
import useLocalStorage from "react-use-localstorage";

import Item from './Item';

import './Admin.scss';

// function expEnded() {
//     if (!localStorage.getItem('exp')) return true;

//     const exp = localStorage.getItem('exp') * 1000;
//     console.log('exp ended?', Date.now() > exp);
//     return Date.now() > exp;
// }

function Products({ changeStatus }) {

    let [mbvToken, setMbvToken] = useLocalStorage('mbv_token');
    // eslint-disable-next-line
    let [mbvExp, setMbvExp] = useLocalStorage('mbv_exp');

    let [search, setSearch] = useState("");
    let [data, setData] = useState([]);
    let [showForm, setShowForm] = useState(false);
    let [text, setText] = useState({
        buy: "",
        delivery: "",
        adv: ""
    });

    useEffect(() => {
        axios({
            method: 'POST',
            url: `${process.env.REACT_APP_SERVER_URL}/products`,
            headers: {
                'Authorization': `Bearer ${mbvToken}`,
            }
        })
            .then(res => {
                console.log('Данные получены', res.data);
                // localStorage.setItem('mbv_exp', res.data.exp);
                setMbvExp(res.data.exp);
                setData(res.data.data);
            })
            .catch(err => {
                console.log(err)
                changeStatus();
            })
        // eslint-disable-next-line
    }, []);

    const onChangeClick = (e) => {

        axios({
            method: 'POST',
            url: `${process.env.REACT_APP_SERVER_URL}/under-text`,
            headers: {
                'Authorization': `Bearer ${mbvToken}`,
            }
        })
            .then(res => {
                console.log('Данные получены', res.data);
                // localStorage.setItem('mbv_exp', res.data.exp);
                setMbvExp(res.data.exp);
                setText(res.data.text);
            })
            .catch(err => {
                console.log(err)
                changeStatus();
            })

        setShowForm(!showForm);
    }

    const changeText = (e) => {
        e.preventDefault();

        const form = e.currentTarget;

        console.log({
            buy: form.elements.buy.value,
            delivery: form.elements.delivery.value,
            adv: form.elements.adv.value,
        })

        axios({
            method: 'POST',
            url: `${process.env.REACT_APP_SERVER_URL}/change-under-text`,
            headers: {
                'Authorization': `Bearer ${mbvToken}`,
            },
            data: {
                buy: form.elements.buy.value,
                delivery: form.elements.delivery.value,
                // adv: form.elements.adv.value,
            }
        })
            .then(res => {
                console.log(res.status);
                setShowForm(false);
            })
            .catch(err => {
                console.log(err);
                changeStatus();
            })
    }

    let filteredData;
    if (search !== "" && data.length > 0) {
        let searchArray = search.split(' ');
        filteredData = data.filter(product => {
            let flag = true;
            searchArray.forEach(word => {
                if (!product.name.toLowerCase().includes(word)) flag = false;
            });
            return flag;
        });
    } else {
        filteredData = data;
    }

    return (
        <div className="Products">
            {/* <Link className="Amin__back" to="/">На сайт</Link> */}
            <div className="Products__logout" onClick={() => {
                // localStorage.removeItem('mbv_token');
                setMbvToken(null);
                setMbvExp(null);
                // localStorage.removeItem('mbv_exp');
                changeStatus();
            }}>Выйти</div>
            <div className="Products__head">
                <div className="Products__search">
                    <input type="text" placeholder="Поиск" onKeyUp={(e) => {
                        if (e.keyCode === 13) {
                            setSearch(e.currentTarget.value.toLowerCase());
                        }
                    }} />
                    <button onClick={(e) => {
                        let value = e.currentTarget.previousSibling.value;
                        if (value.length > 0)
                            setSearch(value.toLowerCase());
                    }}><i className="fas fa-search"></i></button>
                </div>
                <button className="Products__change-text" onClick={onChangeClick}>Изменить текст под товаром</button>
            </div>
            {showForm && <form className="Products__form-change-text" onSubmit={changeText}>
                <label>
                    <div>Как купить?</div>
                    {text.buy !== "" && <textarea name="buy" >{text.buy}</textarea>}
                </label>
                <label>
                    <div>Варианты доставки.</div>
                    {text.delivery !== "" && <textarea name="delivery" >{text.delivery}</textarea>}
                </label>
                {/* <label>
                    <div>Наши преимущества:</div>
                    {text.adv !== "" && <textarea name="adv" >{text.adv}</textarea>}
                </label> */}
                <button type="submit">Изменить</button>
            </form>}
            <ul>
                {filteredData.length > 0 && filteredData.map((item, i) => (
                    <li key={i}>
                        <Item info={item} />
                    </li>
                ))}
                {filteredData.length === 0 && (
                    <p>Пожалуйста, подождите...</p>
                )}
            </ul>
        </div>
    );
}

export default Products;
