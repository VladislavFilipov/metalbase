import React from 'react';
// import ReactDOM from 'react-dom';
// // import { render } from 'react-snapshot';
import { BrowserRouter as Router } from 'react-router-dom';

import './index.scss';
import './media.scss';
import App from './components/App/App';

// ReactDOM.render(<Router><App /></Router>, document.getElementById('root'));

import { hydrate, render } from "react-dom";

const rootElement = document.getElementById("root");
if (rootElement.hasChildNodes()) {
    hydrate(<Router><App /></Router>, rootElement);
} else {
    render(<Router><App /></Router>, rootElement);
}