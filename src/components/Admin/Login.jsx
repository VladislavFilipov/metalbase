import React from 'react';
import axios from 'axios';

import './Admin.scss';


function Login({ changeStatus }) {

    const sendForm = e => {
        e.preventDefault();
        const form = e.currentTarget;

        axios.post(`${process.env.REACT_APP_SERVER_URL}/login`, {
            name: form.elements.name.value,
            password: form.elements.psw.value
        })
            .then(res => {
                // console.log(res.data);
                // localStorage.setItem('token', res.data.token);
                // localStorage.setItem('mbv_token', res.data.token);
                changeStatus(res.data.token);
            })
            .catch(err => console.log(err))
    }

    return (
        <div className="Login">
            <form onSubmit={sendForm}>
                <h1>Войти в панель администратора</h1>
                <input required type="text" name="name" placeholder="Введите имя пользователя" />
                <input required type="password" name="psw" placeholder="Введите пароль" />
                <button type="submit">Войти</button>
            </form>
        </div>
    );
}

export default Login;
