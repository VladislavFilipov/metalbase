import React from 'react';
import { Helmet } from "react-helmet";

import MapComponent from './Map';

import './Contacts.scss';

function Contacts() {

    return (
        <div className="Contacts">
            <Helmet>
                <meta charSet="utf-8" />
                <title>Контакты</title>
                <meta name="fragment" content="!" />
                {/* <link rel="canonical" href="http://mysite.com/example" /> */}
            </Helmet>
            <div itemscope itemtype="http://schema.org/Organization">
                <h1 className="Contacts__title">Контакты</h1>
                <div className="Contacts__mail">
                    <p><b>Почта:</b></p>
                    <p><a href="mailto:info@metallobazav.ru" itemprop="email" >info@metallobazav.ru</a></p>
                </div>
                <div className="Contacts__time">
                    <p><b>График работы металлобазы:</b></p>
                    <p>С 9:00 — до 20:00</p>
                    <p>Без выходных</p>
                </div>
                <div className="Contacts__tel">
                    <p><b>Телефон отдела продаж:</b></p>
                    <p><a href="tel:+78126026206" itemprop="telephone">+7 (812) 325-50-55</a></p>
                </div>
                <div className="Contacts__messengers">
                    <p><b>Мессенджеры:</b></p>
                    <a target="_blank" rel="noopener noreferrer" href="https://msng.link/wa/79213619899">
                        <img alt="Whatsapp" src="https://img.icons8.com/color/48/000000/whatsapp.png" />
                    </a>
                    <a target="_blank" rel="noopener noreferrer" href="https://invite.viber.com/?g2=AQAPJkO4vleg%2F0rBCE2dAWGKoLPHpuuYq%2B0e%2B%2F9tvkUoLWhaLOzpg9sK%2Fr6hUSsP&lang=ru">
                        <img alt="Viber" src="https://img.icons8.com/color/48/000000/viber.png" />
                    </a>
                    <a target="_blank" rel="noopener noreferrer" href="https://msng.link/tg/metallobazav">
                        <img alt="Telegram" src="https://img.icons8.com/color/48/000000/telegram-app.png" />
                    </a>
                </div>
                <div className="Contacts__address">
                    <p>Склад компании «Металлобаза Волхонка» расположен по адресу: <span itemprop="streetAddress">Санкт Петербург, Горелово, Волхонское шоссе, 6</span> , схема проезда приведена на карте:</p>
                </div>
            </div>
            <div className="Contacts__map">
                {/* <iframe title="map" src="https://yandex.ru/map-widget/v1/?um=constructor%3A5be1caae8264e85b723de249bddde462aa28e12b8b76a64cf00c8e7d6ff297ea&amp;source=constructor" width="100%" height="503" frameBorder="0"></iframe> */}
                <MapComponent />
            </div>

            {/* 59.780962, 30.180133 */}
        </div>
    );
}

export default Contacts;
