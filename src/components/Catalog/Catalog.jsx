import React from 'react';

import { Helmet } from "react-helmet";
import ListItem from './ListItem';

import './Catalog.scss';


function Catalog(props) {

    let data = props.data;

    if (data.length === 0) {
        return <div>Пожалуйста, подождите...</div>
    }

    let location = props.location.pathname.slice(8);

    if (location[location.length - 1] === '/') location = location.slice(0, -1);
    console.log(location);
    let search = '', title = '';

    if (/.*s='.*'.*/.test(location)) {
        search = location.split("'")[1];
        title = `Результаты поиска по запросу "${search}"`;
    } else {
        props.categories.forEach(element => {
            if (element.url === location) {
                search = element.search;
                title = element.name;
            }
            else if (element.items) {
                element.items.forEach(item => {
                    if (item.url.includes(location)) {
                        search = item.search;
                        title = item.name;
                    }
                })
            }
        });
    }



    if (search.test) {
        data = data.filter(product => search.test(product.name));
    } else {
        let searchArray = search.toLowerCase().split(' ');
        data = data.filter(product => {
            let flag = true;
            searchArray.forEach(word => {
                if (!product.name.toLowerCase().includes(word)) flag = false;
            });
            return flag;
        });
    }

    return (
        <div className="Catalog">
            <Helmet>
                <meta charSet="utf-8" />
                <title>{title}</title>
                <meta name="fragment" content="!" />
                {/* <link rel="canonical" href="http://mysite.com/example" /> */}
            </Helmet>
            <div className="Catalog__list">
                <h1 className="title">{title}</h1>
                {data.length > 0 ?
                    <div className="head">
                        <span>СОРТАМЕНТ</span>
                        <span>ЦЕНА ЗА ТОННУ</span>
                        <span>КОЛИЧЕСТВО</span>
                        <span>СТОИМОСТЬ</span>
                        <span>КУПИТЬ</span>
                    </div> :
                    <div className="head">Ничего не найдено</div>
                }
                {data.map((product, i) => (
                    <div key={i}>
                        <ListItem product={product} />
                    </div>

                ))}
            </div>
        </div>
    );
}

export default Catalog;
