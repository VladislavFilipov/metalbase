import React, { useState } from 'react';
// import axios from 'axios';

import Login from './Login';
import Products from './Products';
import useLocalStorage from "react-use-localstorage";

import './Admin.scss';
import { Link } from 'react-router-dom';



function Admin() {

    let [status, setStatus] = useState('products');

    let [mbvToken, setMbvToken] = useLocalStorage('mbv_token');

    if ((!mbvToken && status !== 'login')) {
        setStatus('login');
    };



    return (
        <div className="Admin">
            <Link className="Admin__back" to="/">На сайт</Link>

            {status === 'products' ?
                <Products changeStatus={() => setStatus('login')} /> :
                <Login changeStatus={(token) => {
                    setMbvToken(token);
                    setStatus('products');
                }} />}
        </div>
    );
}

export default Admin;
