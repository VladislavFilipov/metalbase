import React, { useState } from 'react';

import './Calc.scss';

function Calc({ mInKg }) {

    let [metrics, setMetrics] = useState({ kg: 1, m: mInKg.toFixed(2) });

    // console.log(mInKg);

    return (
        <div className="Calc">
            <h3 className="title">Калькулятор</h3>
            <label>
                <div>Введите количество килограммов</div>
                <input type="text" name="kg" value={metrics.kg} onChange={(e) => {
                    // setKg(parseInt(e.currentTarget.value) || 0);
                    // setM(kg * mInKg);
                    let tempKg = parseInt(e.currentTarget.value) || 0;
                    setMetrics({
                        kg: tempKg,
                        m: (tempKg * mInKg).toFixed(2)
                    })
                }} />
            </label>

            <label>
                <div>Введите количество метров</div>
                <input type="text" name="m" value={metrics.m} onChange={(e) => {
                    let tempM = parseInt(e.currentTarget.value) || 0;
                    setMetrics({
                        kg: (tempM / mInKg).toFixed(2),
                        m: tempM
                    })
                }} />
            </label>
        </div >
    );
}

export default Calc;
