import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Switch, Route } from 'react-router-dom';

import About from '../About/About';
import Partners from '../Partners/Partners';
import Jobs from '../Jobs/Jobs';
import Contacts from '../Contacts/Contacts';
import Catalog from '../Catalog/Catalog';
import MainPage from '../MainPage/MainPage';
import Product from '../Product/Product';
import Delivery from '../Delivery/Delivery';
import Policy from '../Policy/Policy';

import './Content.scss';

function Content({ categories }) {
    let [data, setData] = useState([]);
    let [text, setText] = useState({});

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_SERVER_URL}/get-data`)
            // axios.get('https://metalbase-server.herokuapp.com/get-data')
            .then(res => {
                // console.log(res.data)
                setData(res.data.data);
                setText(res.data.text);
            })
            .catch(err => console.log(err))
    }, []);

    return (
        <div className="Content">
            <Switch>
                {/* <Route path="/" render={(props) => <MainPage {...props} categories={categories} />} /> */}

                <Route path="/policy" component={Policy} />
                <Route path="/catalog" render={(props) => <Catalog {...props} data={data} categories={categories} />} />
                <Route path="/product/:id" render={(props) => <Product {...props} data={data} text={text} />} />
                <Route path="/delivery" component={Delivery} />
                <Route path="/about">
                    <About />
                </Route>
                <Route path="/partners">
                    <Partners />
                </Route>
                <Route path="/jobs">
                    <Jobs />
                </Route>
                <Route path="/contacts">
                    <Contacts />
                </Route>
                <Route path="/" render={(props) => <MainPage {...props} categories={categories} />} />
            </Switch>
        </div>
    );
}

export default Content;
