import React from 'react';
import { Helmet } from "react-helmet";


import './About.scss';

function About() {
    return (
        <div className="About">
            <Helmet>
                <meta charSet="utf-8" />
                <title>О компании</title>
                <meta name="fragment" content="!" />
                {/* <link rel="canonical" href="http://mysite.com/example" /> */}
            </Helmet>
            <h1 className="About__title">О компании</h1>
            <div className="About__info">
                Здравствуйте.<br /><br />

                Мы - та самая металлобаза, находящаяся на Волхонском шоссе в г. Санкт-Петербург! С 2016 года наша компания осуществляет снабжение высококачественным металлопрокатом на рынке Санкт-Петербурга и Ленинградской области, и уже успела себя зарекомендовать как надежный и ответственный поставщик.<br /><br />

                Партнерами «Металлобазы Волхонка» являются ведущие металлургические комбинаты- ПАО «Северсталь», ПАО «Мечел», ПАО «ММК», ООО «ЕвразХолдинг», АО «ЕВРАЗ Металл Инпром», ПАО «НЛМК». Вся продукция сертифицирована и соответствует ГОСТ.<br /><br />

                В списке наших клиентов более 3000 предприятий и организаций, среди которых не только крупнейшие представители отрасли, но и малый бизнес, и частные лица.<br /><br />

                На данный момент собственный автопарк металлобазы располагает: пятью шаландами (12м/20т), одним автокраном (г/п 25т), тремя «пятитонками» (6.м/5тн) и двумя газелями (6м/1.5т). Поэтому мы легко и точно в срок осуществим доставку на Ваш объект в Санкт-Петербурге или любом населенном пункте Ленинградской области.<br /><br />

                Одним из основных направлений служит проектирование и изготовление строительных металлоконструкций любой степени сложности. Также вы можете заказать у нас сопутствующие услуги: рубка, гибка, распиловка, вальцовка, сверление, резка и покраска металла.<br /><br />

                Благодаря нашей опытной команде профессионалов запрос будет обработан в короткое время. Уже в день обращения вы получите всю необходимую информацию о характеристиках товара и условиях поставки.    <br /><br />
            </div>
            <div className="About__contacts">
                <div className="title">Свяжитесь с нами и мы Вам поможем</div>
                <div className="phone"><i className="fas fa-phone"></i><span>+7 (812) 325-50-55</span></div>
                <div className="mail"><i className="far fa-envelope"></i><span>info@metallobazav.ru</span></div>
                <div className="soc">
                    <a target="_blank" rel="noopener noreferrer" href="https://msng.link/wa/79213619899">
                        <img alt="Whatsapp" src="https://img.icons8.com/color/48/000000/whatsapp.png" />
                    </a>
                    <a target="_blank" rel="noopener noreferrer" href="https://invite.viber.com/?g2=AQAPJkO4vleg%2F0rBCE2dAWGKoLPHpuuYq%2B0e%2B%2F9tvkUoLWhaLOzpg9sK%2Fr6hUSsP&lang=ru">
                        <img alt="Viber" src="https://img.icons8.com/color/48/000000/viber.png" />
                    </a>
                    <a target="_blank" rel="noopener noreferrer" href="https://msng.link/tg/metallobazav">
                        <img alt="Telegram" src="https://img.icons8.com/color/48/000000/telegram-app.png" />
                    </a>
                </div>
            </div>
            <div className="About__adv">
                <div className="title">Наши преимущества</div>
                <ul className="list">
                    {/* <li><i className="fas fa-shuttle-van"></i><span>Собственный автопарк</span></li> */}
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/transport.png`} alt="Собственный автопарк" />
                        <span>Собственный автопарк</span></li>
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/ind_price.png`} alt="Индивидуальная ценовая политика" />
                        <span>Индивидуальная ценовая политика</span></li>
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/sert.png`} alt="Наличие сертификатов на всю поставляемую продукцию" />
                        <span>Наличие сертификатов на всю поставляемую продукцию</span></li>
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/team.png`} alt="Опытная команда профессионалов" />
                        <span>Опытная команда профессионалов</span></li>
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/cat.png`} alt="Широкий сортамент поставляемой продукции" />
                        <span>Широкий сортамент поставляемой продукции</span></li>
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/serv.png`} alt="Предоставление дополнительных услуг" />
                        <span>Предоставление дополнительных услуг</span></li>
                </ul>
            </div>
        </div>
    );
}

export default About;
