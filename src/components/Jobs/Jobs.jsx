import React from 'react';
import './Jobs.scss';
import { Helmet } from "react-helmet";

function Jobs() {

    return (
        <div className="Jobs">
            <Helmet>
                <meta charSet="utf-8" />
                <title>Вакансии</title>
                <meta name="fragment" content="!" />
                {/* <link rel="canonical" href="http://mysite.com/example" /> */}
            </Helmet>
            <h1 className="Jobs__title">Вакансии</h1>
            <div>Компания «Металлобаза СПБ» проводит набор сотрудников по следующим вакансиям:</div>
            <div className="Jobs__job">
                <div className="name">Менеджер по продажам</div>
                <ul className="req">
                    <div>Требования</div>
                    <li>работа с входящими звонками и заявками</li>
                    <li>высшее образование, н/о высшее</li>
                    <li>опыт работы в продажах от полугода</li>
                    <li>знание документооборота</li>
                    <li>уверенный пользователь ПК, работа в CRM</li>
                </ul>
                <ul className="cond">
                    <div>Условия</div>
                    <li>полная занятость</li>
                    <li>полный рабочий день</li>
                    <li>при необходимости — дополнительное обучение</li>
                    <li>оформление по ТК РФ</li>
                    <li>оклад + высокий процент с продаж</li>
                </ul>
            </div>
            <div className="Jobs__job">
                <div className="name">Водитель шаланды</div>
                <ul className="req">
                    <div>Требования</div>
                    <li>водительские права категории Е</li>
                    <li>профильное средне-специальное образование</li>
                    <li>опыт работы от 3 лет</li>
                </ul>
                <ul className="cond">
                    <div>Условия</div>
                    <li>полная занятость</li>
                    <li>работа по Санкт-Петербургу и ЛО</li>
                    <li>оформление по трудовому кодексу РФ</li>
                    <li>фиксированный оклад+ часы</li>
                </ul>
            </div>
            <div className="Jobs__job">
                <div className="name">Водитель бортовой Hynday 6м</div>
                <ul className="req">
                    <div>Требования</div>
                    <li>водительские права категории Е</li>
                    <li>профильное средне-специальное образование</li>
                    <li>опыт работы от 3 лет</li>
                </ul>
                <ul className="cond">
                    <div>Условия</div>
                    <li>полная занятость</li>
                    <li>работа по Санкт-Петербургу и ЛО</li>
                    <li>оформление по трудовому кодексу РФ</li>
                    <li>фиксированный оклад+ часы</li>
                </ul>
            </div>
            <div className="Jobs__job">
                <div className="name">Машинист автокрана</div>
                <ul className="req">
                    <div>Требования</div>
                    <li>профильное средне-специальное образование</li>
                    <li>опыт работы от 3 лет</li>
                </ul>
                <ul className="cond">
                    <div>Условия</div>
                    <li>полная занятость</li>
                    <li>работа на территории металлобазы</li>
                    <li>оформление по ТК РФ</li>
                    <li>фиксированный оклад</li>
                </ul>
            </div>
            <div className="Jobs__job">
                <div className="name">Стропальщик</div>
                <ul className="req">
                    <div>Требования</div>
                    <li>опыт работы от 1 года</li>
                </ul>
                <ul className="cond">
                    <div>Условия</div>
                    <li>полная занятость</li>
                    <li>работа на территории металлобазы</li>
                    <li>оформление по ТК РФ</li>
                    <li>фиксированный оклад</li>
                </ul>
            </div>
            <div><b>Адрес для резюме:</b> info@metallobazav.ru, в теме письма указывайте резюме.</div>
        </div>
    );
}

export default Jobs;
