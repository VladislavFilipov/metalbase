import React from 'react';
import './Header.scss';
import { useHistory, Link } from 'react-router-dom';



function Header() {
    let history = useHistory();

    let changeAddress = (e) => {
        document.querySelectorAll('.Header__nav-item').forEach(item => {
            item.classList.remove('selected');
        });

        e.currentTarget.classList.add('selected');
        e.currentTarget.parentNode.classList.remove('show');

        // history.push(`/${e.currentTarget.dataset.address}`);
    }

    return (
        <div className="Header">
            <div className="Header__name"><Link to="/" className="logo">
                <img src={`${process.env.REACT_APP_SERVER_URL}/static/logo_ng.png`} alt="Металлобаза Волхонка" />
            </Link></div>
            <div className="Header__info">

                <div className="Header__banner">Продажа от 300 кг!</div>
                <div className="Header__time">

                    <span>
                        <div>С 9:00 — до 20:00</div>
                        <div>Без выходных</div>
                    </span>
                    <i className="fas fa-clock"></i>
                </div>
                <div className="Header__contacts">
                    <div><a href="tel:+78123255055">+7 (812) 325-50-55</a><i className="fas fa-phone"></i></div>
                    <div><a href="mailto:info@metallobazav.ru">info@metallobazav.ru</a><i className="far fa-envelope"></i></div>
                    <div className="Header__messengers">
                        <a target="_blank" rel="noopener noreferrer" href="https://msng.link/wa/79213619899">
                            <img alt="Whatsapp" src="https://img.icons8.com/color/48/000000/whatsapp.png" />
                        </a>
                        <a target="_blank" rel="noopener noreferrer" href="https://invite.viber.com/?g2=AQAPJkO4vleg%2F0rBCE2dAWGKoLPHpuuYq%2B0e%2B%2F9tvkUoLWhaLOzpg9sK%2Fr6hUSsP&lang=ru">
                            <img alt="Viber" src="https://img.icons8.com/color/48/000000/viber.png" />
                        </a>
                        <a target="_blank" rel="noopener noreferrer" href="https://msng.link/tg/metallobazav">
                            <img alt="Telegram" src="https://img.icons8.com/color/48/000000/telegram-app.png" />
                        </a>
                    </div>
                </div>
            </div>
            <div className="Header__nav">
                <div className="Header__nav-button" onClick={(e) => {
                    e.currentTarget.nextElementSibling.classList.toggle('show');
                }}>
                    {/* <i className="fas fa-bars"></i> */}
                    <span>Меню<br />сайта</span>
                </div>
                <div className="Header__nav-list">
                    <Link className="Header__nav-item" onClick={changeAddress} to="/"><span > Главная</span></Link>
                    <Link className="Header__nav-item" onClick={changeAddress} to="/about"><span >О компании </span></Link>
                    <Link className="Header__nav-item" onClick={changeAddress} to="/delivery"><span >Доставка </span></Link>
                    {/* <span className="Header__nav-item" onClick={e => {
                        history.push(`/delivery`);
                        changeAddress(e);
                    }}>Доставка </span> */}
                    <Link className="Header__nav-item" onClick={changeAddress} to="/partners"><span >Партнеры</span></Link>
                    <Link className="Header__nav-item" onClick={changeAddress} to="/jobs"><span >Вакансии</span></Link>
                    <Link className="Header__nav-item" onClick={changeAddress} to="/contacts"><span >Контакты</span></Link>
                    {/* <span className="Header__nav-item" onClick={e => {
                        history.push(`/contacts`);
                        changeAddress(e);
                    }}>Контакты </span> */}
                </div>

                <span className="Header__nav-search">
                    <input type="text" placeholder="Поиск" onKeyUp={(e) => {
                        if (e.keyCode === 13) {
                            // console.log('on enter');
                            history.push(`/catalog/s='${e.currentTarget.value}'`);
                        }
                    }} />
                    <i className="fas fa-search" onClick={(e) => {
                        // console.log(e.currentTarget.previousSibling.value)
                        let value = e.currentTarget.previousSibling.value;
                        if (value.length > 0)
                            history.push(`/catalog/s='${value}'`);
                    }}></i>


                </span>
            </div>
        </div>
    );
}

export default Header;
