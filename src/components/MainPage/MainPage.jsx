import React from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from "react-helmet";

import './MainPage.scss';

function MainPage({ categories }) {
    return (
        <div className="MainPage">
            <Helmet>
                <meta charSet="utf-8" />
                <meta name="fragment" content="!" />
                {/* <link rel="canonical" href="http://mysite.com/example" /> */}
            </Helmet>
            <ul className="MainPage__list">
                {categories.map((category, i) => (
                    <Link to={`/catalog${category.url}`} key={i}>
                        <li className="MainPage__list-item">
                            <div className="text">{category.name}</div>
                            <img src={category.img} alt={category.name} />
                        </li>
                    </Link>
                ))}
            </ul>
            <h1 className="MainPage__title">О компании</h1>
            <div className="MainPage__info">
                Мы - та самая металлобаза, находящаяся на Волхонском шоссе в г. Санкт-Петербург! С 2016 года наша компания осуществляет снабжение высококачественным металлопрокатом на рынке Санкт-Петербурга и Ленинградской области, и уже успела себя зарекомендовать как надежный и ответственный поставщик.<br /><br />
            </div>
            <div className="MainPage__adv">
                <div className="title">Наши преимущества</div>
                <ul className="list">
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/transport.png`} alt="Собственный автопарк" />
                        <span>Собственный автопарк</span></li>
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/ind_price.png`} alt="Индивидуальная ценовая политика" />
                        <span>Индивидуальная ценовая политика</span></li>
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/sert.png`} alt="Наличие сертификатов на всю поставляемую продукцию" />
                        <span>Наличие сертификатов на всю поставляемую продукцию</span></li>
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/team.png`} alt="Опытная команда профессионалов" />
                        <span>Опытная команда профессионалов</span></li>
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/cat.png`} alt="Широкий сортамент поставляемой продукции" />
                        <span>Широкий сортамент поставляемой продукции</span></li>
                    <li>
                        <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/serv.png`} alt="Предоставление дополнительных услуг" />
                        <span>Предоставление дополнительных услуг</span></li>
                </ul>
            </div>
        </div>
    );
}

export default MainPage;
