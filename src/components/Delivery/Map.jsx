import React, { useEffect } from 'react';

import axios from 'axios';

var L = require('leaflet');

function MapComponent() {

    useEffect(() => {
        var map = L.map('mapid').setView([59.956430, 30.122871], 10);

        L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        axios.post(`${process.env.REACT_APP_SERVER_URL}/get-geojson`)
            .then(res => {
                L.geoJSON(res.data, {
                    style: function (feature) {
                        return { color: feature.properties.fill };
                    }
                }).addTo(map);
            })
            .catch(err => console.log(err))
    }, []);

    return (
        <div className="MapComponent">
            <div id="mapid"></div>
        </div>
    );
}

export default MapComponent;

