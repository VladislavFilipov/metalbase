import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import ModalBuy from '../ModalBuy/ModalBuy';

import './Catalog.scss';


function ListItem({ product }) {

    let [showModal, setShowModal] = useState(false);
    // let history = useHistory();

    let [value, setValue] = useState(1);
    useEffect(() => {
        setCost((value * product.price).toFixed(2));
    }, [value, product.price])

    let [cost, setCost] = useState(product.price);

    return (
        <div className="Catalog__list-item">
            {showModal && <ModalBuy order={{ productName: product.name, value, price: product.price, cost }} closeModal={() => setShowModal(false)} />}
            <span className="name"><Link to={`/product/${product.url}/`}>{product.name}</Link></span>
            {/* <span className="name" onClick={() => history.push(`/product/${product.url}`)}>{product.name}</span> */}
            <span className="price">
                <div className="mobile">Цена за тонну</div>
                {product.price} руб.
                </span>
            <span className="amount">
                <span className="minus" onClick={() => setValue(value === 1 ? value : value - 1)}>-</span>
                <input type="text" value={value} onChange={(e) => setValue(parseInt(e.currentTarget.value) || 0)} />
                <span className="plus" onClick={() => setValue(value + 1)}>+</span>
            </span>
            <span className="cost">
                <div className="mobile">Стоимость</div>
                {cost} руб.
                </span>
            <button onClick={() => setShowModal(true)}>Купить</button>
        </div>
    );
}

export default ListItem;
