import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './SideMenu.scss';

function showList(e) {

    let target = e.currentTarget;

    if (!target.nextElementSibling) {
        target.parentNode.parentNode.classList.remove('show');
        return;
    }

    let
        contentHeight = parseFloat(getComputedStyle(target.nextElementSibling).height),
        containerHeight = parseFloat(getComputedStyle(target.parentNode).height),
        titleHeight = parseFloat(getComputedStyle(target).height);

    target.querySelector('.arrow').classList.toggle('up');

    if (containerHeight > titleHeight) {
        target.parentNode.style.height = titleHeight + 'px';
        // target.querySelector('.fa-sort-down').classList.remove('up');
    } else {
        target.parentNode.style.height = containerHeight + contentHeight + 'px';
        // target.querySelector('.fa-sort-down').classList.add('up');
    }



    // if (containerHeight <= titleHeight)
}

function SideMenu(props) {
    // let history = useHistory();

    let [opened, setOpened] = useState();

    return (
        <div className="SideMenu">
            {/* <div className="SideMenu__name"><Link to="/"> <img src={`${process.env.REACT_APP_SERVER_URL}/static/logo.png`} alt="" /></Link></div> */}
            <div className="SideMenu__mobile" >
                <span onClick={(e) => {
                    e.currentTarget.parentNode.nextElementSibling.classList.toggle('show');
                }}>Каталог</span>
                <Link to="/delivery"><span >Расчет доставки</span></Link>
                <a href="tel:+78123255055">+7 (812) 325-50-55</a>
            </div>
            <ul className="SideMenu__list">
                {props.categories.map((category, i) => (
                    <li key={i} className={`SideMenu__list-item ${i === opened ? '' : 'closed'}`}>
                        <div
                            className="title"
                            // data-num={i}
                            onClick={(e) => {
                                showList(e);
                                // history.push(`/catalog${category.url}`);
                                setOpened(i);
                            }}
                        >
                            <Link to={`/catalog${category.url}`} className="title-container">
                                <span>{category.name}</span>
                                {category.items && <span className="arrow" ><i className="fas fa-chevron-down"></i></span>}
                            </Link>
                        </div>
                        {category.items && <ul className="list">
                            {category.items.map((item, j) => (
                                <Link key={j} to={`/catalog${item.url}`}>
                                    <li
                                        key={j}
                                        onClick={() => {
                                            document.querySelector('.SideMenu__list').classList.remove('show');
                                            // history.push(`/catalog${item.url}`)
                                        }}
                                    >{item.name}</li>
                                </Link>
                            ))}
                        </ul>}
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default SideMenu;


// class List {
//     constructor(elem) {
//         elem.addEventListener('click', this.onTitleClick.bind(this));
//     }

//     onTitleClick(e) {
//         let target;
//         if (e.target.classList.contains('title'))
//             target = e.target;
//         else
//             return;

//         let
//             contentHeight = parseFloat(getComputedStyle(target.nextElementSibling).height),
//             containerHeight = parseFloat(getComputedStyle(target.parentNode).height),
//             titleHeight = parseFloat(getComputedStyle(target).height);

//         target.querySelector('.arrow').classList.toggle('up');

//         if (containerHeight > titleHeight) {
//             target.parentNode.style.height = titleHeight + 'px';
//             // target.querySelector('.fa-sort-down').classList.remove('up');
//         } else {
//             target.parentNode.style.height = containerHeight + contentHeight + 'px';
//             // target.querySelector('.fa-sort-down').classList.add('up');
//         }
//     }
// }