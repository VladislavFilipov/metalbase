import React, { useEffect } from 'react';

import axios from 'axios';

var L = require('leaflet');
require('leaflet-routing-machine');
require('leaflet-control-geocoder');
require("lrm-google");
var leafletPip = require('@mapbox/leaflet-pip');

let geojson = undefined;

function Test({ returnKm, returnArea }) {

    useEffect(() => {

        var map = L.map('mapid').setView([59.972817, 30.133583], window.screen.width > 1000 ? 10 : 9);

        L.tileLayer(
            "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            // "https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png"
            , {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);

        let control = L.Routing.control({
            waypoints: [L.latLng(59.787358, 30.187762)],
            draggableWaypoints: false,
            geocoderPlaceholder: () => 'Укажите адрес доставки',
            lineOptions: {
                addWaypoints: false,
                styles: [{
                    color: "black"
                }]
            },
            // router: L.Routing.osrmv1({
            //     // serviceUrl: 'http://my-osrm/route/v1',
            //     timeout: 30000
            // }),
            router: new L.Routing.Google(),
            // geocoder: L.Control.Geocoder.nominatim({ serviceUrl: "https://nominatim.openstreetmap.org/" })
            geocoder: L.Control.Geocoder.google('AIzaSyDGNnl2a97MvJwTJma2TutDjhMEXk-Edb0', {
                // collapsed: true, /* Whether its collapsed or not */
                // position: 'topright', /* The position of the control */
                // text: 'Locate', /* The text of the submit button */
                region: 'ru', /* The region code, which alters how it behaves based on a given country or territory */
            })
        })
            .on('routeselected', function (e) {
                var route = e.route;
                // console.log(route.summary.totalDistance / 1000)
                returnKm(route.summary.totalDistance / 1000);
                // whenClicked(e, 'hello');

                // console.log(map)
                // map.fire('click');
                // map._layers[69].fireEvent('click');

                var latlngPoint = new L.LatLng(
                    e.route.actualWaypoints[1].latLng.lat,
                    e.route.actualWaypoints[1].latLng.lng
                );

                console.log(geojson)

                let results = leafletPip.pointInLayer(latlngPoint,
                    geojson);
                console.log(results)

                if (results.length !== 0)
                    returnArea(results[0].feature.id);
                else
                    returnArea(null);
            })
            .addTo(map);

        // function createButton(label, container) {
        //     var btn = L.DomUtil.create('button', '', container);
        //     btn.setAttribute('type', 'button');
        //     btn.innerHTML = label;
        //     return btn;
        // }

        // map.on('click', e => console.log('map click', e));


        map.on('click', function (e) {
            control.spliceWaypoints(control.getWaypoints().length - 1, 1, e.latlng);
            // var container = L.DomUtil.create('div'),
            //     destBtn = createButton('Выбрать точку', container);

            // L.popup()
            //     .setContent(container)
            //     .setLatLng(e.latlng)
            //     .openOn(map);

            // L.DomEvent.on(destBtn, 'click', function () {
            //     control.spliceWaypoints(control.getWaypoints().length - 1, 1, e.latlng);
            //     map.closePopup();
            // });
        });

        axios.post(`${process.env.REACT_APP_SERVER_URL}/get-geojson`)
            .then(res => {
                geojson = L.geoJSON(res.data, {
                    style: function (feature) {
                        return {
                            color: feature.properties.fill, fillOpacity: feature.properties['fill-opacity'], opacity: feature.properties['stroke-opacity']
                        };
                    },
                    // onEachFeature: onEachFeature
                }).addTo(map);
            })
            .catch(err => console.log(err))
        // eslint-disable-next-line
    }, []);

    return (
        <div className="Test">
            <div id="mapid"></div>
        </div>
    );
}

export default Test;

// function whenClicked(e) {
//     // e = event
//     console.log('on geojson clicked', e);
//     returnArea(e.target.feature.id);
//     // You can make your ajax call declaration here
//     //$.ajax(... 
// }

// function onEachFeature(feature, layer) {
//     //bind click
//     layer.on({
//         click: whenClicked
//     });

//     // layer._leaflet_id = feature.id + 1000;
// }