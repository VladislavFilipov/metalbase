import React, { useEffect } from 'react';
import { Switch, Route, useLocation } from 'react-router-dom';
import { Helmet } from "react-helmet";

import Header from '../Header/Header';
import SideMenu from '../SideMenu/SideMenu';
import Content from '../Content/Content';
import Footer from '../Footer/Footer';
import Admin from '../Admin/Admin';
import Banners from '../Banners/Banners';

import './App.scss';

// `${process.env.REACT_APP_SERVER_URL}/static/mainpage/armatura.jpg`

let data = [
    {
        url: '/armatura', name: 'Арматура', search: 'Арматура', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/armatura.jpg`, items: [
            { url: '/armatura/armatura-12-mm', name: 'Арматура 12 мм', search: 'Арматура 12 мм' },
            { url: '/armatura/armatura-a500s', name: 'Арматура А500С', search: 'Арматура А500С' },
            { url: '/armatura/armatura-a240', name: 'Арматура А240', search: 'Арматура А240' },
            { url: '/armatura/armatura-a400-25g2s', name: 'Арматура А400 (25Г2С)', search: 'Арматура А400 (25Г2С)' },
            { url: '/armatura/armatura-35gs', name: 'Арматура 35ГС', search: 'Арматура 35ГС' },
        ]
    },
    {
        url: '/truby-profilnye', name: 'Трубы профильные', search: 'Труба профильная', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/truby-profilnye.jpg`, items: [
            { url: '/truby-profilnye/truby-profilnye-kvadratnye', name: 'Трубы профильные квадратные', search: /Труба профильная ([0-9]+х)\1/ },
            { url: '/truby-profilnye/truby-profilnye-pryamougolnye', name: 'Трубы профильные прямоугольные', search: /Труба профильная ([0-9]+х)(?!\1)/ },
        ]
    },
    {
        url: '/ugolok', name: 'Уголок', search: 'Уголок', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/ugolok.jpg`, items: [
            { url: '/ugolok/ugolok-ravnopolochnyj', name: 'Уголок равнополочный', search: /Уголок.* равнополочный.*/ },
            { url: '/ugolok/ugolok-neravnopolochnyj', name: 'Уголок неравнополочный', search: 'Уголок неравнополочный' },
            { url: '/ugolok/ugolok-gnutyj', name: 'Уголок гнутый', search: 'Уголок гнутый' },
        ]
    },
    { url: '/dvutavr', name: 'Двутавр', search: 'Двутавр', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/dvutavr.jpg`, },
    {
        url: '/shveller', name: 'Швеллер', search: 'Швеллер', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/shveller.jpg`, items: [
            { url: '/shveller/shveller-stalnoj', name: 'Швеллер стальной', search: 'Швеллер стальной' },
            { url: '/shveller/shveller-gnutyj', name: 'Швеллер гнутый', search: 'Швеллер гнутый' },
        ]
    },

    { url: '/list-nerzhaveyushhij', name: 'Лист нержавеющий', search: 'Лист нержавеющий', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/list-nerzhaveyushhij.jpg`, },
    { url: '/list-riflenyj', name: 'Лист рифленый', search: 'Лист рифленый', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/list-riflenyj.jpg`, },
    {
        // url: '/list-stalnoj', name: 'Лист стальной', search: 'Лист стальной', img: 'assets/main/арматура.jpg', items: [
        url: '/list-stalnoj', name: 'Лист стальной', search: /Лист.+(горячекатаный|оцинкованный|холоднокатаный)/, img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/list-stalnoj.jpg`, items: [
            { url: '/list-stalnoj/list-goryachekatanyj', name: 'Лист горячекатаный', search: 'Лист горячекатаный' },
            { url: '/list-stalnoj/list-otsinkovannyj', name: 'Лист оцинкованный', search: 'Лист оцинкованный' },
            { url: '/list-stalnoj/list-holodnokatanyj', name: 'Лист холоднокатаный', search: 'Лист холоднокатаный' },
        ]
    },
    {
        url: '/setka-stalnaya', name: 'Сетка стальная', search: 'Сетка', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/setka-stalnaya.jpg`, items: [
            { url: '/setka-stalnaya/setka-svarnaya', name: 'Сетка сварная', search: 'Сетка сварная' },
            { url: '/setka-stalnaya/setka-pletenaya', name: 'Сетка плетеная', search: 'Сетка плетеная' },
            { url: '/setka-stalnaya/setka-tkanaya', name: 'Сетка тканая', search: 'Сетка тканая' },
        ]
    },
    {
        url: '/kvadrat', name: 'Квадрат', search: 'Квадрат', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/kvadrat.jpg`, items: [
            { url: '/kvadrat/kvadrat-stalnoj', name: 'Квадрат стальной', search: 'Квадрат стальной' },
            { url: '/kvadrat/kvadrat-kalibrovannyj', name: 'Квадрат калиброванный', search: 'Квадрат калиброванный' },
        ]
    },
    {
        url: '/krug', name: 'Круг', search: 'Круг', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/krug.jpg`, items: [
            { url: '/krug/krug-stalnoj', name: 'Круг стальной', search: 'Круг стальной' },
            { url: '/krug/krug-kalibrovannyj', name: 'Круг калиброванный', search: 'Круг калиброванный' },
        ]
    },
    { url: '/polosa', name: 'Полоса', search: 'Полоса', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/polosa.jpg`, },




    {
        url: '/truby-besshovnye', name: 'Трубы бесшовные', search: 'Труба бесшовная', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/truby-besshovnye.jpg`, items: [
            { url: '/truby-besshovnye/truby-goryachedeformirovannye', name: 'Трубы горячедеформированные', search: 'Труба горячедеформированная' },
            { url: '/truby-besshovnye/truby-holodnodeformirovannye', name: 'Трубы холоднодеформированные', search: 'Труба холоднодеформированная' },
        ]
    },
    { url: '/truby-vgp', name: 'Трубы ВГП', search: 'Труба ВГП', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/truby-vgp.jpg`, },
    {
        url: '/truby-nerzhaveyushhie', name: 'Трубы нержавеющие', search: 'Труба нержавеющая', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/truby-nerzhaveyushhie.jpg`, items: [
            { url: '/truby-nerzhaveyushhie/truby-nerzhaveyushhie-kruglye', name: 'Трубы нержавеющие круглые', search: 'Труба нержавеющая круглая' },
            { url: '/truby-nerzhaveyushhie/truby-nerzhaveyushhie-kvadratnye', name: 'Трубы нержавеющие квадратные', search: /Труба нержавеющая (.+)х\1х.+/ },
            { url: '/truby-nerzhaveyushhie/truby-nerzhaveyushhie-pryamougolnye', name: 'Трубы нержавеющие прямоугольные', search: /Труба нержавеющая .+х.+х.+/ },
        ]
    },
    { url: '/truby-elektrosvarnye', name: 'Трубы электросварные', search: 'Труба электросварная', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/truby-elektrosvarnye.jpg`, },
    {
        url: '/provoloka', name: 'Проволока', search: 'Проволока', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/provoloka.jpg`, items: [
            { url: '/provoloka/provoloka-vr-1', name: 'Проволока Вр-1', search: 'Проволока Вр-1' },
            { url: '/provoloka/provoloka-vyazalnaya', name: 'Проволока вязальная', search: 'Проволока вязальная' },
            { url: '/provoloka/provoloka-otozhzhennaya', name: 'Проволока отожженная', search: 'Проволока отожженная' },
            { url: '/provoloka/provoloka-otsinkovannaya', name: 'Проволока оцинкованная', search: 'Проволока оцинкованная' },
            { url: '/provoloka/provoloka-stalnaya', name: 'Проволока стальная', search: 'Проволока стальная' },
        ]
    },
    { url: '/prosechno-vytyazhnoj-list', name: 'Просечно-вытяжной лист', search: 'Просечно-вытяжной лист', img: `${process.env.REACT_APP_SERVER_URL}/static/mainpage/pros-vit-list.png`, }
]


function App() {
    // let history = useHistory();
    let location = useLocation();

    useEffect(() => {

        document.addEventListener('scroll', () => {
            const up = document.querySelector('.App__up');

            if (window.pageYOffset > 1000) {
                // console.log(1);
                up.classList.add('show');
            } else {
                // console.log(2);
                up.classList.remove('show');
            }
        })
    }, []);

    return (
        <div className="App">
            <Helmet>
                <meta charSet="utf-8" />
                <title>Металлобаза Волхонка</title>
                <meta name="fragment" content="!" />
                <meta name="description"
                    content="Мы - та самая металлобаза, находящаяся на Волхонском шоссе в г. Санкт-Петербург! С 2016 года наша компания осуществляет снабжение высококачественным металлопрокатом на рынке Санкт-Петербурга и Ленинградской области, и уже успела себя зарекомендовать как надежный и ответственный поставщик." />
            </Helmet>
            <Switch>
                <Route path="/admin"><Admin /></Route>
                <Route path="/">


                    {/* <div className="App__body"> */}
                    <Header />
                    {location.pathname === '/' && <Banners />}
                    <SideMenu categories={data} />
                    <Content categories={data} />

                    {/* </div> */}
                    <Footer />
                </Route>
            </Switch>
            <button className="App__up" onClick={() => window.scroll(0, 0)}>
                <i className="fas fa-chevron-up"></i>
                {/* <div>Наверх</div> */}
            </button>
        </div>
    );
}

export default App;
