import React, { useState } from 'react';
import axios from 'axios';
import useLocalStorage from "react-use-localstorage";

import './Admin.scss';

function Item({ info }) {

    let [showForm, setShowForm] = useState(false);
    // eslint-disable-next-line
    let [mbvToken, setMbvToken] = useLocalStorage('mbv_token');

    const sendChanges = (e) => {
        e.preventDefault();
        const form = e.currentTarget;

        axios({
            method: 'POST',
            url: `${process.env.REACT_APP_SERVER_URL}/change-product`,
            data: Object.assign(info, { price: form.elements.price.value }),
            headers: {
                'Authorization': `Bearer ${mbvToken}`,
            }
        })
            .then(res => {
                console.log(res.data);
                setShowForm(false);
            })
            .catch(err => {
                console.log(err)
            })

        console.log(Object.assign(info, { price: form.elements.price.value }));
    }

    return (
        <div className="Item">
            <div className="Item__info">
                <span className="name">{info.name}</span>
                <span>{info.price}</span>
                <button onClick={() => setShowForm(!showForm)}>Редактировать</button>
            </div>
            {showForm &&
                <form className="Item__form" onSubmit={sendChanges}>
                    <label><span>Изменить цену товара:</span><input type="number" name="price" defaultValue={info.price} /></label>
                    <button type="submit">Принять</button>
                </form>
            }
        </div>
    );
}

export default Item;
