import React, { useState, useEffect } from 'react';
import parse from 'html-react-parser';
import { Helmet } from 'react-helmet';

import ModalBuy from '../ModalBuy/ModalBuy';
import Calc from '../Calc/Calc';

import './Product.scss';

function clearStr(str) {
    let newStr = str.replace(/<h2 style="text-align: center;"><span style="font-size:15pt;"><strong><span style="font-family:Arial,Helvetica,sans-serif;">Способы оплаты<\/span><\/strong><\/span><\/h2>[^]*/, '');
    newStr = newStr.replace(/<h2 style="text-align: center;"><strong>Характеристики<\/strong><\/h2>/g, '');
    newStr = newStr.replace(/<p style="text-align: center;"> <\/p>/g, '');
    // \n<p style="text-align: center;"> <\/p>
    newStr = newStr.replace(/style=".*"/g, '');
    newStr = newStr.replace(/<p>\s*<\/p>/g, '');
    newStr = newStr.replace(/p/g, 'li');
    // newStr = newStr.replace(/<ul>|<\/ul>/, '');
    return newStr;
}

function Product(props) {

    let [showModal, setShowModal] = useState(false);
    let [meters, setMeters] = useState();


    let [product, setProduct] = useState();
    useEffect(() => {
        setProduct(props.data.find(item => item.url === props.match.params.id));

    }, [props]);

    let [value, setValue] = useState(1);
    useEffect(() => {
        setCost(product ? (value * product.price).toFixed(2) : 0);

        // console.log(product)
        if (product && product.params) {
            let temp = product.params.match(/Количество метров в 1 тонне:.*\s(.+)м./);
            console.log(1, temp)
            if (temp) {
                setMeters(+temp[1]);
                // temp = product.price / temp[1];
                // console.log(2, temp)
            }
        }

    }, [value, product])

    let [cost, setCost] = useState(product ? product.price : 0);

    if (!product) {
        return (
            <div>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title> цена за метр и тонну, купить в СПБ, продажа оптом.</title>
                    <meta name="description" content={`Купить по выгодной цене оптом и в розницу в комтании Металлобаза Волхонка. Доставка по СПБ и ЛО с оплатой по факту на объекте.`} />
                    <meta name="fragment" content="!" />
                    {/* <link rel="canonical" href="http://mysite.com/example" /> */}
                </Helmet>
                Пожалуйста, подождите...
            </div>
        );
    }

    return (
        <div className="Product">
            <Helmet>
                <meta charSet="utf-8" />
                <title>{product.name} цена за метр и тонну, купить в СПБ, продажа оптом.</title>
                <meta name="description" content={`Купить ${product.name} по выгодной цене оптом и в розницу в комтании Металлобаза Волхонка. Доставка по СПБ и ЛО с оплатой по факту на объекте.`} />
                <meta name="fragment" content="!" />
                {/* <link rel="canonical" href="http://mysite.com/example" /> */}
            </Helmet>
            {showModal && <ModalBuy order={{ productName: product.name, value, price: product.price, cost }} closeModal={() => setShowModal(false)} />}
            <h1 className="Product__name"><u>{product.name}</u></h1>
            {/* <div className="Product__desc">Купить <u>{product.name}</u> по выгодной цене оптом и в розницу в комтании Металлобаза Волхонка. Доставка по СПБ и ЛО с оплатой по факту на объекте.</div> */}
            <div className="Product__img"><img src={product.img} alt={product.name} /></div>
            <div className="Product__card">
                <span className="Product__price"><b>Цена за тонну </b><span>{product.price} руб.</span> </span>
                <span className="Product__amount">
                    <b>Количество</b>
                    <span>
                        <span className="minus" onClick={() => setValue(value === 1 ? value : value - 1)}>-</span>
                        <input type="text"
                            value={value}
                            onChange={(e) => setValue(parseInt(e.currentTarget.value) || 0)}
                        />
                        <span className="plus" onClick={() => setValue(value + 1)}>+</span>
                    </span>
                </span>
                <span className="Product__cost"><b>на сумму</b><span>{cost} руб.</span></span>
                <button onClick={() => setShowModal(true)}>Купить</button>
            </div>

            {meters && <Calc mInKg={meters / 1000} />}
            <div className="Product__contacts">
                <p>Связаться с нами</p>
                <ul>
                    <li>+7 (812) 325-50-55</li>
                    <li>info@metallobazav.ru</li>
                    <li><div className="messengers">
                        <a target="_blank" rel="noopener noreferrer" href="https://msng.link/wa/79213619899">
                            <img alt="Whatsapp" src="https://img.icons8.com/color/48/000000/whatsapp.png" />
                        </a>
                        <a target="_blank" rel="noopener noreferrer" href="https://invite.viber.com/?g2=AQAPJkO4vleg%2F0rBCE2dAWGKoLPHpuuYq%2B0e%2B%2F9tvkUoLWhaLOzpg9sK%2Fr6hUSsP&lang=ru">
                            <img alt="Viber" src="https://img.icons8.com/color/48/000000/viber.png" />
                        </a>
                        <a target="_blank" rel="noopener noreferrer" href="https://msng.link/tg/metallobazav">
                            <img alt="Telegram" src="https://img.icons8.com/color/48/000000/telegram-app.png" />
                        </a>
                    </div>
                    </li>
                </ul>
            </div>
            {
                product.params && <div className="Product__params">
                    <h2>Характеристики</h2>
                    {parse(clearStr(product.params))}
                </div>
            }
            <div className="Product__info">
                <h2>Как купить?</h2>

                {
                    props.text.buy.split('\n').map((item, i) => (
                        <p key={i}>{item}</p>
                    ))
                }
                {/* <p>Заявка > Консультация > Оформление заказа > Доставка > Приемка товара > Оплата</p><br />
                <p>Оплата наличными, переводом на карту или переводом на расчетный счет.</p> */}

                <h2>Варианты доставки.</h2>

                {
                    props.text.delivery.split('\n').map((item, i) => (
                        <p key={i}>{item}</p>
                    ))
                }
                {/* <p><b>Нашим транспортом:</b> шаланды, бортовые машины, газели, газоны.</p><br />
                <p><b>Самовывоз</b> с нашей металлобазы по адресу: Санкт-Петербург, Волхонское шоссе, 4.</p> */}

                {/* <h2>Наши преимущества:</h2> */}

                <div className="Product__adv">
                    <div className="title">Наши преимущества</div>
                    <ul className="list">
                        <li>
                            <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/transport.png`} alt="Собственный автопарк" />
                            <span>Собственный автопарк</span></li>
                        <li>
                            <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/ind_price.png`} alt="Индивидуальная ценовая политика" />
                            <span>Индивидуальная ценовая политика</span></li>
                        <li>
                            <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/sert.png`} alt="Наличие сертификатов на всю поставляемую продукцию" />
                            <span>Наличие сертификатов на всю поставляемую продукцию</span></li>
                        <li>
                            <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/team.png`} alt="Опытная команда профессионалов" />
                            <span>Опытная команда профессионалов</span></li>
                        <li>
                            <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/cat.png`} alt="Широкий сортамент поставляемой продукции" />
                            <span>Широкий сортамент поставляемой продукции</span></li>
                        <li>
                            <img className="icon" src={`${process.env.REACT_APP_SERVER_URL}/static/plus/serv.png`} alt="Предоставление дополнительных услуг" />
                            <span>Предоставление дополнительных услуг</span></li>
                    </ul>
                </div>
                {/* <ul>
                    {
                        props.text.adv.split('\n').map((item, i) => (
                            <li key={i}>{item}</li>
                        ))
                    }
                </ul> */}

                <div className="offer">
                    <p>Информация на сайте не является публичной офертой.</p>
                    <p>Актуальную стоимость и наличие товара уточняйте с помощью формы обратной связи или по телефону: 8 (812) 325-50-55</p>
                </div>
            </div>
        </div >
    );
}

export default Product;
