import React, { useState } from 'react';
import axios from 'axios';

import './ModalCall.scss';

function ModalCall({ closeModal }) {

    let [status, setStatus] = useState('fill');

    let sendData = (e) => {
        e.preventDefault();
        const form = e.currentTarget;

        if (
            !form.elements.name.value ||
            !form.elements.tel.value ||
            !form.elements.policy.checked
        ) {
            setStatus('error');
            return;
        }

        console.log({
            name: form.elements.name.value,
            tel: form.elements.tel.value
        })

        axios.post('https://metalbase-server.herokuapp.com/send-mail-to-call', {
            name: form.elements.name.value,
            tel: form.elements.tel.value
        })
            .then(res => {
                console.log('успех')
                setStatus('success');
            })
            .catch(err => console.log(err))
    }

    return (
        <div className="ModalCall">
            <form className="ModalCall__form" onSubmit={sendData}>
                <h3>Заказать обратный звонок</h3>
                <input type="text" name="name" placeholder="Введите имя" />
                <input type="text" name="tel" placeholder="Введите номер телефона" />
                <label>
                    <input type="checkbox" name="policy" />
                    <span>Согласен с политикой конфиденциальности</span>
                </label>
                {status === 'error' && <p>Заполните все поля!</p>}
                {status !== 'success' && <button type="submit">Заказать звонок</button>}
                {status === 'success' && <p>Успешно!</p>}

                <div className="close" onClick={() => closeModal()}>+</div>
            </form>
        </div>
    );
}

export default ModalCall;
