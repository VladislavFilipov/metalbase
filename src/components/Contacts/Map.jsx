import React, { useEffect } from 'react';

var L = require('leaflet');

function MapComponent() {

    useEffect(() => {
        var map = L.map('mapid-contacts').setView([59.780962, 30.180133], 13);

        L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        L.marker([59.780962, 30.180133]).addTo(map);
    }, []);

    return (
        <div className="MapComponent">
            <div id="mapid-contacts"></div>
        </div>
    );
}

export default MapComponent;

